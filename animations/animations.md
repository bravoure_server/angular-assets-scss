

# How to use the Jello animation libary

We have a set of basic animations for different types of elements. Below is a list of these types:

    - Buttons
    - Links
    - Icons
    - Images
    
The _animation-base.scss file contains a list of all type of animations already assigned to their own classes.

##- Animation variables

There are some standard variables which can be extended in your custom project (the first type is the default animation):


    - $animate-icon-type (left / top / right / bottom)
    - $animate-link-type (left / right / bottom / double-left / double-right)
    - $animate-button-type (left / top / right / bottom)
    - $animate-image-type (zoom / top / right / bottom / left)
    
    - $animate-base-speed (.6s)
    - $animate-base-border-size (2px)
    
    
##- Assign classes

###- General classes:

Skew: **.animate-skew-forward**

###- Icon classes:

Direction: **.animate-icon-direction**

###- Link classes:

Underline direction: **.animate-link-underline**

###- Button classes:

Fill direction: **.animate-button-fill**

###- Image classes:

Add the class to the parent element of the image, so when you hover over the element, the image will hover behind it.

Fill direction: **.animate-image-position**
