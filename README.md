
# Assets Scss - Bravoure Component

This Component adds the general scss files to the project


### Installation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-assets-scss": "1.0"
      }
    }

and then run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    

